FROM python:3-alpine

WORKDIR /code

COPY ./requirements.txt /code
RUN pip install -r requirements.txt

RUN mkdir db
COPY . /code

ARG COMMIT_TAG
ARG COMMIT_SHA

RUN sed -i "s/%%%BUILDTAG%%%/$COMMIT_TAG/" blog/templates/blog/base.html
RUN sed -i "s/ff9400/$COMMIT_SHA/" blog/static/css/blog.css

EXPOSE 8000

CMD sh init.sh && python manage.py runserver 0.0.0.0:8000
